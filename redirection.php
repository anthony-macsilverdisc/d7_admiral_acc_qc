<?php
error_reporting ( 0 );
$ini_array = parse_ini_file ( "redirection.ini", true );

$httpcode = array (
		"100" => "Continue",
		"101" => "Switching Protocols",
		"102" => "Processing (WebDAV; RFC 2518)",
		"200" => "OK",
		"201" => "Created",
		"202" => "Accepted",
		"203" => "Non-Authoritative Information (since HTTP/1.1)",
		"204" => "No Content",
		"205" => "Reset Content",
		"206" => "Partial Content",
		"207" => "Multi-Status (WebDAV; RFC 4918)",
		"208" => "Already Reported (WebDAV; RFC 5842)",
		"226" => "IM Used (RFC 3229)",
		"300" => "Multiple Choices",
		"301" => "Moved Permanently ....",
		"302" => "Found",
		"303" => "See Other (since HTTP/1.1)",
		"304" => "Not Modified",
		"305" => "Use Proxy (since HTTP/1.1)",
		"306" => "Switch Proxy",
		"307" => "Temporary Redirect (since HTTP/1.1)",
		"308" => "Permanent Redirect (Experimental RFC; RFC 7238)",
		"404" => "error on Polish Wikipedia",
		"400" => "Bad Request",
		"401" => "Unauthorized",
		"402" => "Payment Required",
		"403" => "Forbidden",
		"404" => "Not Found",
		"405" => "Method Not Allowed",
		"406" => "Not Acceptable",
		"407" => "Proxy Authentication Required",
		"408" => "Request Timeout",
		"409" => "Conflict",
		"410" => "Gone",
		"411" => "Length Required",
		"412" => "Precondition Failed",
		"413" => "Request Entity Too Large",
		"414" => "Request-URI Too Long",
		"415" => "Unsupported Media Type",
		"416" => "Requested Range Not Satisfiable",
		"417" => "Expectation Failed",
		"418" => "I'm a teapot (RFC 2324)",
		"419" => "Authentication Timeout (not in RFC 2616)",
		"420" => "Method Failure (Spring Framework)",
		"420" => "Enhance Your Calm (Twitter)",
		"422" => "Unprocessable Entity (WebDAV; RFC 4918)",
		"423" => "Locked (WebDAV; RFC 4918)",
		"424" => "Failed Dependency (WebDAV; RFC 4918)",
		"426" => "Upgrade Required",
		"428" => "Precondition Required (RFC 6585)",
		"429" => "Too Many Requests (RFC 6585)",
		"431" => "Request Header Fields Too Large (RFC 6585)",
		"440" => "Login Timeout (Microsoft)",
		"444" => "No Response (Nginx)",
		"449" => "Retry With (Microsoft)",
		"450" => "Blocked by Windows Parental Controls (Microsoft)",
		"451" => "Unavailable For Legal Reasons (Internet draft)",
		"451" => "Redirect (Microsoft)",
		"494" => "Request Header Too Large (Nginx)",
		"495" => "Cert Error (Nginx)",
		"496" => "No Cert (Nginx)",
		"497" => "HTTP to HTTPS (Nginx)",
		"498" => "Token expired/invalid (Esri)",
		"499" => "Client Closed Request (Nginx)",
		"499" => "Token required (Esri)",
		"500" => "Internal Server Error",
		"501" => "Not Implemented",
		"502" => "Bad Gateway",
		"503" => "Service Unavailable",
		"504" => "Gateway Timeout",
		"505" => "HTTP Version Not Supported",
		"506" => "Variant Also Negotiates (RFC 2295)",
		"507" => "Insufficient Storage (WebDAV; RFC 4918)",
		"508" => "Loop Detected (WebDAV; RFC 5842)",
		"509" => "Bandwidth Limit Exceeded (Apache bw/limited extension)[25]",
		"510" => "Not Extended (RFC 2774)",
		"511" => "Network Authentication Required (RFC 6585)",
		"520" => "Origin Error (Cloudflare)",
		"521" => "Web server is down (Cloudflare)",
		"522" => "Connection timed out (Cloudflare)",
		"523" => "Proxy Declined Request (Cloudflare)",
		"524" => "A timeout occurred (Cloudflare)",
		"598" => "Network read timeout error (Unknown)",
		"599" => "Network connect timeout error (Unknown)" 
);

if ($_SERVER ["SERVER_NAME"] == $ini_array ["domain"] ["SERVERNAME"]) {
	$domain = $ini_array ["domain"] ["PROTOCOL"]."://" . $_SERVER ["SERVER_NAME"] . $_SERVER ["REQUEST_URI"];
	$instruction = searchRedirect ( $domain, $ini_array );
	if ($instruction !== 0) {
		$message_http = "HTTP/1.1 " . $instruction [3] . " " . $httpcode [$instruction [3]] . " ";
		Header ( $message_http );
		Header ( "Location: $instruction[2]" );
		exit ();
	}
}

include "index.php";


/*FUNCTIONS*/
function searchRedirect($domain, $ini_array) {
	// print_r($domain);
	// qdie($ini_array);
	$fila = 1;
	if (($gestor = fopen ( $ini_array ["filesystem"] ["CSVREDIRECTION"], "r" )) !== FALSE) {
		while ( ($datos = fgetcsv ( $gestor, 1000, "," )) !== FALSE ) {
			$numero = count ( $datos );
			if ($numero == 4) {
				if ($datos [0] == "url") {
					if ($domain == $datos [1]) {
						fclose ( $gestor );
						return $datos;
					}
				}
				if ($datos [0] == "idcat") {
					$querystring = "idcat=" . $datos [1];
					if (stripos ( $domain, $querystring )) {
						fclose ( $gestor );
						return $datos;
					}
				}
			}
		}
		fclose ( $gestor );
	} else {
		qdie ( "ERROR: open file - " . $ini_array ["filesystem"] ["CSVREDIRECTION"] );
	}
	
	$dummys = array (
			"error",
			null,
			$ini_array ["domain"] ["PROTOCOL"]."://" . $ini_array ["domain"] ["SERVERNAME"] . "/",
			"301" 
	);
	
	// analizar: url contiene idcat=XXX => redirect /
	if (stripos ( $domain, "idcat=" )) {
		return $dummys;
	}
	// analizar: url contiene idpro=XXX => redirect /
	if (stripos ( $domain, "idpro=" )) {
		return $dummys;
	}
	// analizar: url si es .asp, .htm => redirect /
	
	$urlArr = parse_url ( $_SERVER ['REQUEST_URI'] );
	$url_array = explode ( "/", $urlArr ["path"] );
	$extension_array = explode ( '.', $url_array [count ( $url_array ) - 1] );
	$extension = $extension_array [count ( $extension_array ) - 1];
	
	if (in_array ( $extension, $ini_array ["validation"] ["EXTENSION_ALLOW"] )) {
		return $dummys;
	}
	/* retorna control a drupal */
	return 0;
}


function qdie($instruction) {
	echo "<pre>";
	print_r ( $instruction );
	echo "</pre>";
	//exit ();
}



?>
