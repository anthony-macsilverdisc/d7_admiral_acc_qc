<body>
<div id="left-panel" style="padding-top:212px;max-height: 1737px; height: 1737px; width: 2480px; float: left;">
    <div id="left-panel-image">200
        <? if (isset($horizontal_left_panel_image) && $horizontal_left_panel_image != '') { ?>
        <div style="width:1472px; height:826px; margin-left:auto;margin-right:auto;text-align:center">
            <?php echo $horizontal_left_panel_image ?>
        </div>
        <? }  ?>
    </div>
</div>
<div id="right-panel" style="padding-top:140px;max-height:1737px;height:1737px;width:2480px;float:left;">
     <div id="greeting" style="height:774px;width:2240px;margin-bottom:40px;margin-left:auto;margin-right:auto;background:white;">
         <div style="width:2240px;">
                 <div style="text-align:left;width:2240px;height:774px;">
                    <? if ($greeting != '') { ?>
                        <?php echo $greeting ?>
                    <? } ?>
                  </div>
         </div>
     </div>
    <div id="address" style="text-align:center;height:296px;width:1201px;margin-left: auto; margin-right: auto; background:white;">
        <?php echo $address ?>
    </div>
    <div id="bottom-wrapper" style="width:2474px;margin-left:auto;margin-right:auto;vertical-align:bottom;">
        <table style="width:2474px;">
            <tr><td style="padding-left: 81px;" align="left" width="586" height="333" valign="bottom">
                <? if ($left_bottom != '') { ?>
                    <div id="left-image" style="height:293px; width:586px;"><?php echo $left_bottom ?></div>
                <? } ?>
                </td>
                <td align="right" style="padding-right:81px;" width="586" height="333" valign="bottom">
                <? if ($right_bottom != '') { ?>
                    <div id="right-image" style="height:293px; margin-right:81px; width:586px;"><?php echo $right_bottom ?></div>
                <? } ?>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>