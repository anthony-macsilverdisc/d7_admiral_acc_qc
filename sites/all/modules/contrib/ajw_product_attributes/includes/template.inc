<?php

class Template
{
    protected $variables = array();

    public function __get($name) {
        return $this->variables[$name];
    }

    public function __set($name, $value) {
        $this->variables[$name] = $value;
    }

    public function render($template) {

        if(array_key_exists($template, $this->variables)) {
            throw new Exception("Cannot use variable called $template");
        }

        extract($this->variables);
        ob_start();
        include($template);
        return ob_get_clean();
    }
}