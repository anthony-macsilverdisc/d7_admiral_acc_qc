This module controls the volume pricing for admiral cards

The requirement was 50 cards @ $1.99 then subsequent cards @ 0.99

It creates an action which is used by Rules. The action effectively adds a negative value
to the base_price

To use this module install and then create a pricing rule.
Select Removes x amount based on Admiral pricing from the actions list.

Set the quantity where the discount breakpoint starts
Set the new price for the discounted quantity.

Make sure that the rule is weighted above any VAT calculations.