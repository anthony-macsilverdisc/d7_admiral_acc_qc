<?php
/**
 * Implementation of hook_rules_action_info()
 */
function admiral_ten_rules_action_info(){
    return array(
        'admiral_ten_apply_discount' => array( // This is the name of the callback function
            'label' => t('Apply 10% discount(time-based)'),
            'group' => t('Commerce Line Item'),
            'parameter' => array(
                'line_item' => array(
                    'type' => 'commerce_line_item',
                    'label' => t('Line Item'),
                    'description' => t('The line item on which to apply the volume discount.'),
                    'wrapped' => true, // This is nice, it will be wrapped for us
                    'save' => true,    // And saved too!
                ),
                'discount_percentage' => array(
                    'type' => 'decimal',
                    'label' => t('Discount Percentage'),
                    'description' => t('Specify a discount percentage'),
                ),
            ),
        ),
    );
}


/**
 * Rules action that applies a volume discount to an order
 * @param commerce_line_item $line_item
 * @return type
 */
function admiral_ten_apply_discount($line_item_wrapper, $discount_percentage){
    //dsm($line_item_wrapper->value()->type);
    if($line_item_wrapper->value()->type == 'ajw_product_attributes_line_item_custom'){
        // Find the total discount on the line item
        $discount_quantity = $line_item_wrapper->quantity->value();
        $discount_total_ammount = $discount_quantity * $line_item_wrapper->commerce_unit_price->amount->value() * ($discount_percentage/100);

        // The discount is applied to the unit price, so we need to make sure
        // our discount is per unit
        $discount = $discount_total_ammount / $line_item_wrapper->quantity->value();

        if($discount){
            // Add the discount as a price component
            $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
                $line_item_wrapper->commerce_unit_price->value(),
                'ten_discount',
                array(
                    'amount' => $discount * -1,
                    'currency_code' => 'GBP',
                    'data' => array()
                ),
                0 // NOT included already in the price
            );
        }
    }
}