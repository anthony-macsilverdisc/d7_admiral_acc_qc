<?php


// Add IE conditional css

function admiral_preprocess_html(&$variables) {
  drupal_add_css(drupal_get_path('theme', 'admiral') . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE', '!IE' => FALSE), 'preprocess' => FALSE));
}