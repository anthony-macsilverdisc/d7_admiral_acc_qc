<?php

/**
 * @file
 * Contains a pre-process hook for 'comment'.
 */

/**
 * Implements hook_preprocess_node().
 *
 */
function admiral_preprocess_node(&$variables) {

    if ($variables['type'] == 'blog_post' && $variables['view_mode'] == 'full') {

        //Building out some extra info needed for the blog post pages
        $user_pic_html = theme_get_setting('toggle_node_user_picture') ? theme('user_picture', array('account' => $variables['node'])) : '';
        $user_pic_html = str_replace('View user profile.', 'View Team Member blog posts.', $user_pic_html);
        $user_pic_html = str_replace('/users/', '/team-member/', $user_pic_html);

        $variables['user_picture'] = $user_pic_html;


        //Get the user id who made the blog post
        $doc_user_id = $variables['node']->uid;

        //Load this user
        $doc_user = entity_load_single('user', $doc_user_id);
        //Get the member info
        $wrapper = entity_metadata_wrapper('user', $doc_user);
        $member_info = $wrapper->field_team_member_info->value(array('decode' => TRUE));

        $variables['member_info'] = '<div class="member_info"><p>' . $member_info . '</p></div>';
        $variables['user_post_info'] = '<p class="user-post-info">Posted by <span class="user_post_info_name">';
        $variables['user_post_info'] .= $variables['node']->name;
        $variables['user_post_info'] .= '</span></p>';
        $variables['formatted_date'] = '<p class="formatted_date">on ';
        $variables['formatted_date'] .= format_date($variables['created'], 'custom', 'jS F Y');
        $variables['formatted_date'] .= '</p>';

    }

  if ($variables['type'] == 'testimonials') {
    //Add a new variable to show client details of the testimonial
    $variables['client_details'] = '';
    if (isset($variables['field_test_name'][0]['safe_value'])) {
      $variables['client_details'] .= $variables['field_test_name'][0]['safe_value'];
    }

    if (isset($variables['field_test_location'][0]['safe_value']) && isset($variables['field_test_name'][0]['safe_value'])) {
      $variables['client_details'] .= ', ' . $variables['field_test_location'][0]['safe_value'];
    }
    else {
      if (isset($variables['field_test_location'][0]['safe_value'])) {
        $variables['client_details'] .= $variables['field_test_location'][0]['safe_value'];
      }
    }

    if ($variables['client_details'] !='' && isset($variables['field_test_company_name'][0]['safe_value'])) {
      $variables['client_details'] .= ', ' . $variables['field_test_company_name'][0]['safe_value'];
    }
  }
}